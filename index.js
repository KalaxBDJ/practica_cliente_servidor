const express = require('express');
var colors = require(`colors`);
const environment = require('./environment')

const port = environment.port ;

const server = express();

server.get("",(req,res)=>{
    res.send("Hola, Gracias por usar mi servidor")
});

server.get("/ruta1",(req,res)=>{
    res.send("Estas accediendo a la ruta #1")
})

server.listen(port,()=>{
    console.log(colors.white("Server on port: "),colors.green(port))
})