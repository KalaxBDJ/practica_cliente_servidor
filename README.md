# Cliente - Servidor

![Cliente-Servidor](/src/cliente.png)

# Anotaciones
Si desea replicar el ejercicio deberá tener en cuenta lo siguiente:

1. Tener instalado [Node.Js](https://nodejs.org/es/download/ "NodeJs")
2. Tener [git](https://git-scm.com/ "Git") instalado en el equipo (Recomendado)

## Importante
Al clonar el repositorio o descargarlo recuerde que este esta subido sin los paquetes ya que son bastante pesados, así que debera instalarlos usted mismo.

A continución le mostrare los comandos que debera ingresar en la consola para la descarga de los paquetes, recuerde estar parado sobre la carpeta.

```
npm i express colors
```

Al ejecutar el comando, automaticamente comenzara la descarga de los paquetes, por favor espere a que finalize la descarga.

Ya con esto debería estar listo para iniciar el servidor, el cual se inicia con el siguiente comando:

```
npm start
```

![ejemplo](/src/ejemplo.png)

Para tumbar el servidor oprima Ctrl+C 2 veces.

### By: Santiago Salazar Castaño

